import { readable } from 'svelte/store'

const getTime = () => new Date().getTime()

export const time = readable(getTime(), set => {
  let running = true
  const step = () => {
    set(getTime())
    if (running) requestAnimationFrame(step)
  }
  
  step()

	return () => running = false
});